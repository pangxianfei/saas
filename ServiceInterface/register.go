package ServiceInterface

import (
	sysmdel "gitee.com/pangxianfei/saas/sysmodel"
	"gorm.io/gorm"
)

type register interface {
	GetByMobile(db *gorm.DB, mobile string) *sysmdel.PlatformAdmin
	GetBySubMobile(db *gorm.DB, mobile string) *sysmdel.TenantUser
	SynTenantUser(admin *sysmdel.PlatformAdmin) (*sysmdel.TenantUser, error)
}
